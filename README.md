# pollbot_admin

### Creating new React app

___

#### requirements (linux)

1. node.js (npm included): [node.js installation site](https://nodejs.org/ko/)

2. yarn (package manager): [yarn installation site](https://classic.yarnpkg.com/en/docs/install#windows-stable)

   ```
   $ npm install --global yarn
   ```

   

#### Create new project and run development server

```
$ npx create-react-app [project name]
e.g. $ npx create-react-app my-app

$ cd [project name]
$ npm start or yarn start
```



### Production Build Deployment

___

#### requirements(linux)

1. node version manager(nvm)

   ```
   $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
   ```

   - nvm: command not found 뜰 경우

     ```
     export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
     [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
     ```

2. node.js (v14.15.5, npm included)

   ```
   $ nvm install 14.15.5
   ```


#### requirements(window)

1. node js (web에서 download)

#### Deploying new production build

1. (새 빌드 배포 시) 기존 build 삭제

   ```
   $ rm -r /repo/build
   ```

2. pull pollbot_admin

   ```
   $ cd /pollbot_admin
   ```

   ```
   $ git pull origin main
   ```

3. production build 생성

   ```
   $ cd ~/frontend
   ```

   ```
   $ npm install (package.json 의 depndency 설치)
   $ npm run build
   ```

4. 생성된 build 옮기기

   ```
   $ mv /pollbot_admin/frontend/build /repo/build
   ```

5. 실행 및 확인 (http://221.168.32.165:5000/)

   ```
   $ cd /repo
   ```

   ```
   $ flask run --host=0.0.0.0
   ```

   ```
   Chat.js 의 componentDidMount() 확인
    componentDidMount() {
           // add resize Listener
           this.fixDiscussionsWidth();
           window.addEventListener("resize", this.fixDiscussionsWidth.bind(this));
   
           const sensorEndpoint = "http://221.168.32.165:5000";
           this.socket = io.connect(sensorEndpoint, {
               reconnection: true,
               withCredentials: true,
           });
           console.log("component mounted");
   ```
   
   