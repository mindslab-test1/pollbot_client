import React, { Component } from "react";

export default class Discussion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      selectedUserIdx: 0,
      selectedCaller: "",
    };
  }

  componentDidMount() {
    this.setState({
      userList: this.props.userList,
      selectedUserIdx: this.props.selectedUserIdx,
    });
  }

  selectedUserInfoHandle = (idx) => (e) => {
    e.stopPropagation();
    if (idx === this.state.selectedUserIdx) {
      return;
    }

    let clickedCaller = e.target.innerText;

    this.props.onClick({
      selectedUserIdx: idx,
      selectedCaller: clickedCaller,
    });

    this.setState({
      ...this.state,
      selectedUserIdx: idx,
      selectedCaller: clickedCaller,
    });
  };

  render() {
    const userList = this.props.userList;
    const selectedUserIdx = this.props.selectedUserIdx;
    const searchKeyword = this.props.searchKeyword;
    const searchResult = this.props.searchResult;

    if (searchKeyword && searchResult.length > 0) {
      return (
        <div className="disc-msg-container">
          {}
          {searchResult.map((user, idx) => {
            return (
              <div
                className={
                  idx === selectedUserIdx
                    ? "discussion message-active"
                    : "discussion"
                }
                key={idx}
                onClick={this.selectedUserInfoHandle(idx)}
              >
                <div className="desc-contact d-flex align-items-center">
                  <p className="name">{user.caller}</p>
                </div>
                <div className="dropdown">
                  <i
                    className="dropdown-icon dropdown clickable fa fa-ellipsis-v"
                    data-toggle="dropdown"
                    aria-hidden="true"
                    aria-haspopup="true"
                    aria-expanded="false"
                  ></i>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <p className="dropdown-item">세션전환</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      );
    } else if (searchKeyword && searchResult.length === 0) {
      return (
        <div className="disc-msg-container">
          <div className="discussion">
            <div className="d-flex align-items-center">
              <p className="name">No Search Results</p>
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className="disc-msg-container">
          {userList.map((user, idx) => {
            return (
              <div
                className={
                  idx === selectedUserIdx
                    ? "discussion message-active"
                    : "discussion"
                }
                key={idx}
                onClick={this.selectedUserInfoHandle(idx)}
              >
                <div className="desc-contact d-flex align-items-center">
                  <p className="name">{user.caller}</p>
                </div>
                <div className="dropdown">
                  <i
                    className="dropdown-icon clickable fa fa-ellipsis-v"
                    data-toggle="dropdown"
                    // aria-hidden="true"
                    aria-haspopup="true"
                    aria-expanded="false"
                    id={user.caller}                   
                  ></i>
                  <div
                    className="dropdown-menu"
                    aria-labelledby={user.caller}
                  >
                    <p className="dropdown-item">세션전환</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      );
    }
  }
   
}
