import React, { Component } from "react";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

export default class Status extends Component {
  constructor(props) {
    super(props);
    this.messageRef = React.createRef();
  }

  scrollToEnd() {
      this.messageRef.current.scrollIntoView({
        behavior: "smooth",
        block: "end",
      });
    
  }

  componentDidUpdate() {
    if (this.props.userstt.length > 0) {
      this.scrollToEnd();
    }
  }

  render() {
    const selectedCaller = this.props.selectedCaller;
    const stt = this.props.userstt;
    //console.log(">>> stt: ", stt);
    const state = this.props.callstate;

    // polbot and caller
    if (stt.caller_id_number === selectedCaller && "result" in stt) {
      return (
        <div className="footer-chat">
          <p className="name"><i className="icon fa fa-commenting-o" aria-hidden="true" ></i>&nbsp;STT:&nbsp;{stt.result}</p>
          <div className="response time">
            {/* <p ref={this.messageRef}> */}
            <p>
              {stt.datetime} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {stt.caller_id_number},{stt.dnis}
            </p>
          </div>
        </div>
      )
    }
    else {
      return (
        <div className="footer-chat">
          <p className="name"><i className="icon fa fa-phone-square" aria-hidden="true" ></i>&nbsp;콜상태:&nbsp;{state.callstate}</p>
          <div className="response time">
            <p>
              {state.datetime} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {state.caller_id_number},{state.dnis}
            </p>
          </div>
        </div>
      )
    }
  }
}


