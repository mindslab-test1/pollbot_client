import React, { Component } from "react";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import polbotIcon from "./polbot_icon.png";

export default class Pages extends Component {
  constructor(props) {
    super(props);
    this.messageRef = React.createRef();
    this.state = {
      writtenMessage: "",
    };
  }

  scrollToEnd() {
      this.messageRef.current.scrollIntoView({
        behavior: "smooth",
        block: "end",
      });
    
  }

  componentDidUpdate() {
//    if (this.props.dataList.length > 0 && this.props.selectedCaller !== "") {
    if (this.props.userchat.length > 0) {
      this.scrollToEnd();
    }
  }

  onChange = (e) => {
    let message = e.target.value;
    // console.log(message);
    this.setState({
      writtenMessage: message,
    });
  };

  onEnterKeyDown = (e) => {
    if (e.key === "Enter") {
      let message = e.target.value.trim();
      if (message === "") {
        console.log("empty message");
        return;
      }

      const chat = this.props.userchat;
      if (chat.callstate !== 'ACTIVE') {
        console.log(">>> message at invalid state: ", chat.callstate);
        return;
      }
      const timezoneOffset = new Date().getTimezoneOffset() * 60000;
      const datetime = new Date(Date.now() - timezoneOffset);

      let user_input = {
        uuid: chat.uuid,
        callee_id_number: chat.callee_id_number,
        caller_id_number: chat.caller_id_number,
        datetime: datetime.toISOString().replace("T", " "),
        isAgent: chat.isAgent,
        isWebEvent: true,
        result: message,
        text: message
      };
      console.log(">>> user_input: ", user_input);
      this.mysocket.emit("my event", user_input);

      this.setState({
        //dataList: this.state.dataList.concat(user_input),
        writtenMessage: "",
      });
    }
  };

  writeMessage = (e) => {
    let writtenMessage = this.state.writtenMessage.trim();
    if (writtenMessage === "") {
      console.log("empty message");
      return;
    }

    const chat = this.props.userchat;
    if (chat.callstate !== 'ACTIVE') {
      console.log(">>> message at invalid state: ", chat.callstate);
      return;
    }

    const timezoneOffset = new Date().getTimezoneOffset() * 60000;
    const datetime = new Date(Date.now() - timezoneOffset);
    let currentTime = new Date().toLocaleString("en-US", { hour12: false });
    let user_input = {
      uuid: chat.uuid,
      callee_id_number: chat.callee_id_number,
      caller_id_number: chat.caller_id_number,
      datetime: datetime.toISOString().replace("T", " "),
      isAgent: chat.isAgent,
      isWebEvent: true,
      result: writtenMessage,
      text: writtenMessage,
    };
    console.log(">>> user_input: ", user_input);
    this.props.mysocket.emit("my event", user_input);

    this.setState({
      //dataList: this.state.dataList.concat(user_input),
      writtenMessage: "",
    });
  };

  sendWebEvent(id, i) {
    const chat = this.props.userchat;
    //console.log("event: " + id + ", " + i)
    let digit = chat.div[id].menu[i].digit;
    let result = chat.div[id].menu[i].stt_hint[0];
    this.props.mysocket.emit("my event", {
      uuid: chat.uuid,
      digit: digit,
      result: result,
      text: result,
      index: [id, i],
    });
  }

  render() {
    const selectedCaller = this.props.selectedCaller;
    const chat = this.props.userchat;
    //console.log(">>> userchat: ", chat);
    const stt = this.props.userstt;
    //console.log(">>> userstt: ", stt);

    // polbot and caller
    if (chat.caller_id_number === selectedCaller && "div" in chat) {
      return (
        <div className="messages-chat">
          <p className="title"><i className="icon fa fa-credit-card" aria-hidden="true"></i>&nbsp;&nbsp;{chat.info.title}</p>
            {chat.div.map((item, id) => {
            //console.log(">>> item.text: ", item.text)
            if (item.menu) {
              //console.log(">>> item.menu: ", item.menu)
              return (
                <div>
                  <div className="message">
                    <p className="text">{item.text}</p>
                  </div>
                  <div>
                    {
                      item.menu.map((subitem, i) => {
                        //console.log(">>> subitem.text: ", subitem.text)
                        if (stt.answer === item.text && subitem.stt_hint.indexOf(stt.result) !== -1) {
                          return (
                            <button className="text" id={i} style={{backgroundColor:"#8bc34a"}} onClick={(e) => {
                                e.preventDefault();
                                //console.log(">>> event: ", e)
                                this.sendWebEvent(id, i);
                            }}>{subitem.text}</button>
                          )
                        }
                        else {
                          return (
                            <button className="text" id={i} onClick={(e) => {
                                e.preventDefault();
                                //console.log(">>> event: ", e)
                                this.sendWebEvent(id, i);
                            }}>{subitem.text}</button>
                          )
                        }
                      })
                    }
                  </div>
                </div>
              )
            }
            if (item.input) {
              return (
                <div>
                  <div className="message">
                    <p className="text">{item.text}</p>
                  </div>
                  <div className="message">
                    <input
                      type="text"
                      className="text"
                      onChange={this.onChange}
                      value={this.state.writtenMessage}
                      onKeyDown={this.onEnterKeyDown}
                      placeholder={item.input.placeholder}
                    ></input>
                    <button className="text icon fa fa-paper-plane-o clickable" onClick={(e) => {
                      //console.log(">>> event: ", e)
                      e.preventDefault();
                      this.writeMessage();
                    }}>전송</button>
                    {/* <i
                      className="icon send fa fa-paper-plane-o clickable"
                      aria-hidden="true"
                      onClick={(e) => {
                          e.preventDefault();
                      }}
                    ></i> */}
                  </div>
                </div>
              )
            }
          })}
        </div>
      )
    }
    else {
      return (
        <div className="messages-chat">
          {/* <div className="message-polbot"></div> */}
            <p className="title"><i className="icon fa fa-exclamation-triangle" aria-hidden="true" ></i>&nbsp;No call data to display</p>
        </div>
      )
    }
  }

}

