import React, { Component } from "react";

import "./Visio.css";

import io from "socket.io-client";

import Pages from "./Pages";
import Status from "./Status";
import Discussion from "./Discussion";
export default class Visio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userList: [],
            selectedUserIdx: 0,
            selectedCaller: "",
            writtenMessage: "",
            isSocketOn: false,
            disconnectCaller: "",
            searchKeyword: "",
            searchResult: [],
            userdata: [],
            userchat: [],
            userstt: [],
            callstate: [],
            socket: null,

            // discussions section style
            discussionsVisibility: "hidden",
            discussionsWidth: "0px",
            searchbarVisibility: "hidden",
            showDiscBtnStyle: { color: "black", cursor: "pointer" },
        };
    }

    componentWillUnmount() {
        this.socket.on("disconnect", () => {
            this.socket.close();
        });

        // remove resizeListener
        window.removeEventListener(
            "resize",
            this.fixDiscussionsWidth.bind(this)
        );
        console.log("component unmounted");
    }

    componentDidMount() {
        // add resize Listener
        this.fixDiscussionsWidth();
        window.addEventListener("resize", this.fixDiscussionsWidth.bind(this));

        const sensorEndpoint = "http://221.168.32.165:5000";
        this.socket = io.connect(sensorEndpoint, {
            reconnection: true,
            withCredentials: true,
        });
        console.log("component mounted");

        if (this.state.isSocketOn === false) {
            this.socket.on("connect", () => {
                this.socket.emit("my event", {
                    data: "Polbot Admin Connected!!!",
                });

                // To access io socket object from pages.js and status.js
                this.setState({
                    socket: this.socket,
                })
                this.socket.on("my response", (res) => {
                    console.log(res);
                    this.setState({
                        isSocketOn: true,
                    });
                });
            });
        }
        this.handleSocketData();
    }

    saveUsers = (user, dataList) => {
        let userList = this.state.userList;
        let dupUser = userList.some((item) => item.caller_id_number === user.caller_id_number);

        if (!dupUser) {
            userList = userList.concat(user);
        }

        let selectedCaller = "";
        if (this.state.selectedCaller === "") {
            selectedCaller = userList[0].caller_id_number;
        } else {
            selectedCaller = this.state.selectedCaller;
        }

        this.setState({
            ...this.state,
            userList: userList,
            dataList: dataList,
            selectedCaller: selectedCaller,
        });
    };

    handleSocketData = () => {
        this.socket.on("my chat", (res) => {
            console.log(">>>>> my-chat");
            console.log(res);

            let user = {
                uuid: res.uuid,
                caller_id_number: res.caller_id_number,
                callee_id_number: res.callee_id_number,
            };

            this.saveUsers(user);
            this.setState({
                userchat: res,
            });
        });

        this.socket.on("my stt", (res) => {
            console.log(">>>>> my-stt");
            console.log(res);

            let user = {
                uuid: res.uuid,
                caller_id_number: res.caller_id_number,
                callee_id_number: res.callee_id_number,
            };

            this.saveUsers(user);
            this.setState({
                userstt: res,
                callstate: [],
            });
        });

        this.socket.on("my callstate", (res) => {
            console.log(">>>>>> my-callstate");
            console.log(res);
            if (res.callstate === "HANGUP") {
                this.setState({
                    userchat: [],
                    userstt: [],
                    callstate: res,
                });
            }
            else {
                this.setState({
                    userstt: [],
                    callstate: res,
                });
            }
        });
    }

    fixDiscussionsWidth() {
        if (this.state.discussionsVisibility === "visible") {
            if (window.innerWidth >= 768) {
                this.setState({
                    discussionsWidth: "412px",
                });
            } else {
                this.setState({
                    discussionsWidth: "280px",
                });
            }
        }
    }

    showDiscussions() {
        if (this.state.discussionsVisibility === "hidden") {
            this.setState({
                discussionsVisibility: "visible",
                showDiscBtnStyle: { color: "lightgray", cursor: "default" },
            });
            if (window.innerWidth >= 768) {
                this.setState({
                    discussionsWidth: "412px",
                });
            } else {
                this.setState({
                    discussionsWidth: "280px",
                });
            }
            setTimeout(() => {
                this.setState({
                    searchbarVisibility: "visible",
                });
            }, 100);
        }
    }

    hideDiscussions() {
        if (this.state.discussionsVisibility === "visible") {
            this.setState({
                discussionsVisibility: "hidden",
                discussionsWidth: "0px",
                searchbarVisibility: "hidden",
                showDiscBtnStyle: { color: "black", cursor: "pointer" },
            });
        }
    }

    onChange = (e) => {
        let message = e.target.value;
        // console.log(message);
        this.setState({
            writtenMessage: message,
        });
    };

    onEnterKeyDown = (e) => {
        if (e.key === "Enter") {
            let message = e.target.value.trim();
            if (message === "") {
                console.log("empty message 1");
                return;
            }

            const timezoneOffset = new Date().getTimezoneOffset() * 60000;
            const datetime = new Date(Date.now() - timezoneOffset);
            const user_data = {
                callee: "None",
                caller: this.state.selectedCaller,
                result: message,
                uuid: "None",
                datetime: datetime.toISOString().replace("T", " "),
                isConsultant: "False",
            };
            this.setState({
                userdata: user_data,
                writtenMessage: "",
            });
        }
    };

    writeMessage = (e) => {
        let writtenMessage = this.state.writtenMessage.trim();
        if (writtenMessage === "") {
            console.log("empty message 2");
            return;
        }

        let currentTime = new Date().toLocaleString("en-US", { hour12: false });
        let user_data = {
            callee: "None",
            caller: this.state.selectedCaller,
            result: writtenMessage,
            uuid: "None",
            datetime: currentTime,
            isConsultant: "False",
        };

        this.setState({
            userdata: user_data,
            writtenMessage: "",
        });
    };

    selectedUserInfoHandle = (idx) => (e) => {
        e.stopPropagation();
        if (idx === this.state.selectedUserIdx) {
            return;
        }

        let clickedCaller = e.target.innerText;
        this.setState({
            ...this.state,
            selectedUserIdx: idx,
            selectedCaller: clickedCaller,
        });
    };

    selectedUserHandler = (userInfo) => {
        console.log(">>>>> selectedUserHandler");
        console.log(userInfo);

        this.setState({
            ...this.state,
            selectedUserIdx: userInfo.selectedUserIdx,
            selectedCaller: userInfo.selectedCaller,
        });
    };

    onSearchKeywordChange = (e) => {
        const userList = this.state.userList;
        this.setState(
            {
                searchKeyword: e.target.value,
            },
            () => {
                this.searchCaller(userList, this.state.searchKeyword);
            }
        );
    };

    searchCaller = (originalList, keyword) => {
        if (keyword) {
            let userFound = originalList.some((user) =>
                user.caller_id_number.includes(keyword)
            );
            if (userFound) {
                let tmpUserList = originalList.filter((user) =>
                    user.caller_id_number.includes(keyword)
                );
                this.setState({
                    searchResult: tmpUserList,
                });
            } else {
                this.setState({
                    searchResult: [],
                });
            }
        } else {
            this.setState({
                searchResult: [],
            });
        }
    };

    onSearchOff() {
        this.setState({
            searchKeyword: "",
            searchResult: [],
        });
    }

    render() {
        const userchat = this.state.userchat;
        const userstt = this.state.userstt;
        const callstate = this.state.callstate;
        const userList = this.state.userList;
        const selectedUserIdx = this.state.selectedUserIdx;
        const selectedCaller = this.state.selectedCaller;
        const searchKeyword = this.state.searchKeyword;
        const searchResult = this.state.searchResult;
        const mysocket = this.state.socket;

        return (
            <div>
                <div className="main d-flex flex-row">
                    {/* menu */}
                    <nav className="menu">
                        <ul className="items">
                            <li
                                className="item item-active"
                                onClick={(e) => {
                                    e.preventDefault();
                                    alert("clicked nav menu item");
                                }}
                            >
                                <i
                                    className="fa fa-commenting"
                                    aria-hidden="true"
                                ></i>
                            </li>
                        </ul>
                    </nav>

                    {/* discussions */}
                    <section
                        className="discussions"
                        style={{
                            visibility: this.state.discussionsVisibility,
                            width: this.state.discussionsWidth,
                            transition: "0.3s",
                        }}
                    >
                        <div className="discussion search">
                            <div
                                className="searchbar"
                                style={{
                                    visibility: this.state.searchbarVisibility,
                                }}
                            >
                                <i
                                    className="fa fa-search"
                                    aria-hidden="true"
                                ></i>
                                <input
                                    type="text"
                                    placeholder="Search..."
                                    value={this.state.searchKeyword}
                                    onChange={this.onSearchKeywordChange}
                                ></input>
                                {this.state.searchKeyword ? (
                                    <i
                                        className="fa fa-times search-delete"
                                        aria-hidden="true"
                                        onClick={(e) => {
                                            this.onSearchOff();
                                        }}
                                    ></i>
                                ) : (
                                    <></>
                                )}
                            </div>
                            <i
                                className="hide-disc-button icon clickable fa fa-minus"
                                aria-hidden="true"
                                style={{
                                    visibility: this.state
                                        .discussionsVisibility,
                                }}
                                onClick={(e) => {
                                    e.preventDefault();
                                    this.hideDiscussions();
                                }}
                            ></i>
                        </div>

                        <Discussion
                            searchKeyword={searchKeyword}
                            searchResult={searchResult}
                            userList={userList}
                            selectedUserIdx={selectedUserIdx}
                            selectedCaller={selectedCaller}
                            onClick={this.selectedUserHandler}
                        />
                    </section>

                    {/* chat */}
                    <section className="chat">
                        <div className="header-chat">
                            <i
                                className="show-disc-button icon clickable fa fa-bars"
                                aria-hidden="true"
                                style={this.state.showDiscBtnStyle}
                                onClick={(e) => {
                                    e.preventDefault();
                                    this.showDiscussions();
                                }}
                            ></i>
                            <p className="name">보이는 음성봇 by 마인즈랩</p>
                        </div>
                        <Pages
                            userchat={userchat}
                            userstt={userstt}
                            callstate={callstate}
                            selectedCaller={selectedCaller}
                            mysocket={mysocket}
                        />
                        <Status
                            userstt={userstt}
                            callstate={callstate}
                            selectedCaller={selectedCaller}
                        />
                    </section>
                </div>
            </div>
        );
    }
}
