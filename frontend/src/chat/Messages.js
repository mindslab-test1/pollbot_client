import React, { Component } from "react";

import polbotIcon from "./polbot_icon.png";
import consultantIcon from "./consultant.png";

export default class Messages extends Component {
  constructor(props) {
    super(props);
    this.messageRef = React.createRef();
  }

  scrollToEnd() {
      this.messageRef.current.scrollIntoView({
        behavior: "smooth",
        block: "end",
      });
    
  }

  componentDidUpdate() {
    if (this.props.dataList.length > 0 && this.props.selectedCaller !== "") {
      this.scrollToEnd();
    }
  }

  render() {
    const selectedCaller = this.props.selectedCaller;

    return (
      <div className="messages-chat">
        {this.props.dataList.map((data, idx) => {
          // Indication for RINGING and HANGUP call state 
          if ("callstate" in data && data.caller_id_number === selectedCaller) {
            if (data.callstate === "RINGING" || data.callstate === "HANGUP") {
              return (
                <div
                  className="message-callstate"
                  ref={this.messageRef}
                  key={idx}
                >
                  <p className="timestamp">~~~~~ {data.callstate}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[{data.datetime}] ~~~~~</p>
                </div>
              );
            }
          }
          // polbot and caller
          if (data.isAgent === false && "result" in data && data.caller_id_number === selectedCaller) {
            return (
              <div className="message-caller" key={idx}>
                <div className="message text-only">
                  <div className="response">
                    <p className="text">{data.result}</p>
                  </div>
                </div>
                <p className="response-time time" ref={this.messageRef}>
                  {" "}
                  {data.datetime ? (
                    <>
                      {data.datetime.slice(0, 23)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
                      {data.caller_id_number}
                    </>
                  ) : (
                    <></>
                  )}
                </p>
              </div>
            );
          }
          if (data.isAgent === false && "answer" in data && data.caller_id_number === selectedCaller) {
            return (
              <div className="message-polbot" key={idx}>
                <div className="message">
                  <div
                    className="photo"
                    style={{ backgroundImage: `url(${polbotIcon})` }}
                  >
                    <div className="online"></div>
                  </div>
                  <p className="text">{data.answer}</p>
                </div>
                <p className="time" ref={this.messageRef}>
                  {" "}
                  {data.datetime ? <>{data.dnis} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  {data.datetime.slice(0, 23)}
                </> : <></>}
                </p>
              </div>
            );
          }
          // consultant and caller
          if (data.isAgent === true && data.callee_id_name === "agent" && data.caller_id_number === selectedCaller) {
            return (
              <div className="message-consultant" key={idx}>
                <div className="message">
                  <div
                    className="photo"
                    style={{ backgroundImage: `url(${consultantIcon})` }}
                  >
                    <div className="online"></div>
                  </div>
                  <p className="message-consultant-text text">{data.result}</p>
                </div>
                <p className="time" ref={this.messageRef}>
                  {" "}
                  {data.datetime ? <>{data.callee_id_number} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  {data.datetime.slice(0, 23)}
                  </> : <></>}
                </p>
              </div>
            );
          }
          if (data.isAgent === true && data.caller_id_name === data.caller_id_number && data.caller_id_number === selectedCaller) {
            return (
              <div className="message-caller" key={idx}>
                <div className="message text-only">
                  <div className="response">
                    <p className="text">{data.result}</p>
                  </div>
                </div>
                <p className="response-time time" ref={this.messageRef}>
                  {" "}
                  {data.datetime ? <>{data.datetime.slice(0, 23)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  {data.caller_id_number}
                  </> : <></>}
                </p>
              </div>
            );
          }
        })}
      </div>
    );
  }
}
